var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)
var actions = require('../actions/Actions')

app.get('/', (req, res) => {
    res.sendFile('E:/sisteme distribuite/sd/gym-frontend/frontend/src/reusable_components/chat/MessageList.js')
})

//Whenever someone connects this gets executed
io.on('connection', socket => {
 
    // save message in db and send acknowledge to client
    socket.on('message', data => {
        actions.sendMessage(data)
        .then(response => io.sockets.emit('event', response))
    })

    socket.on('delete', () => {
        actions.clearChat()
        .then(result => io.sockets.emit('acknowledge', result))
    })

    socket.on('subscriptionChosed', subscriptionType => {
        actions.findSubscriptionByType(subscriptionType)
        .then(response => io.sockets.emit('subscriptionResponse', response))
    })

    //Whenever someone disconnects this piece of code executed
    socket.on('disconnect', () => {});
});

// ther server listen on port 2999
http.listen(2999, () => {})