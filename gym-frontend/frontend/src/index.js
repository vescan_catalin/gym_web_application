import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css'

import Login from './pages/login_register/Login'
import Users from './pages/informations/user_informations/Users'
import Trainers from './pages/informations/trainer_informations/Trainers'
import Admins from './pages/informations/admin_informations/Admins'
import Home from './pages/informations/home_page_info/Home'
import Schedule from './pages/informations/home_page_info/Schedule'
import UserSchedule from './pages/informations//user_informations/UserSchedule'
import FindUs from './pages/informations/home_page_info/FindUs'
import MyAccount from './pages/informations/user_informations/MyAccount'
import SubscriptionInformations from './pages/informations/home_page_info/SubscriptionInformations'
import Photos from './pages/informations/home_page_info/Photos'
import Products from './pages/informations/home_page_info/Products'
import TrainerAccountInformations from './pages/informations/trainer_informations/TrainerAccountInformations'
import AdminSchedule from './pages/informations/admin_informations/AdminSchedule'
import TrainerSchedule from './pages/informations/trainer_informations/TrainerSchedule'
import Notifications from './pages/informations/admin_informations/Notifications'
import GuestsChat from './pages/informations/home_page_info/GuestsChat'
import AdminChat from './pages/informations/admin_informations/AdminChat'
import TrainerChat from './pages/informations/trainer_informations/TrainerChat'
import AdminSubscriptionInformations from './pages/informations/admin_informations/AdminSubscriptionInformations'
import ViewTrainers from './pages/informations/user_informations/ViewTrainers';
import Reservations from './pages/informations/user_informations/Reservations';
import Purchase from './pages/informations/user_informations/Purchase';
import UsersDetails from './pages/informations/trainer_informations/UsersDetails';
import UsersInformations from './pages/informations/admin_informations/UsersInformations';
import ViewTrainersInformations from './pages/informations/home_page_info/ViewTrainersInformations';
import TrainersInformations from './pages/informations/admin_informations/TrainersInformations';
import ProductsInformations from './pages/informations/admin_informations/ProductsInformations';
import ViewReservations from './pages/informations/trainer_informations/ViewReservations';

export default class App extends React.Component {
	render() {
		return(
		<main>
			<Switch>
				{/* home paths */}
				<Route exact path = '/' component = {Home}/>
				<Route path = '/schedule' component = {Schedule}/>
				<Route path = '/viewTrainers' component = {ViewTrainersInformations}/>
				<Route path = '/subscription' component = {SubscriptionInformations}/>
				<Route path = '/photos' component = {Photos}/>
				<Route path = '/products' component = {Products}/>
				<Route path = '/findUs' component = {FindUs}/>
				<Route path = '/chat' component = {GuestsChat}/>
				<Route path = '/login' component = {Login}/>

				{/* users paths */}
				<Route path = '/users/myAccount' component = {MyAccount}/>
				<Route path = '/users/purchase' component = {Purchase}/>
				<Route path = '/users/reservations' component = {Reservations}/>
				<Route path = '/users/viewTrainers' component = {ViewTrainers}/>				
				<Route path = '/users/schedule' component = {UserSchedule}/>
				<Route path = '/users' component = {Users}/>
				
				{/* admins paths */}
				<Route path = '/admins/notifications' component = {Notifications}/>
				
				<Route path = '/admins/chat' component = {AdminChat}/>
				<Route path = '/admins/products' component = {ProductsInformations}/>
				<Route path = '/admins/subscription' component = {AdminSubscriptionInformations}/>
				<Route path = '/admins/users' component = {UsersInformations}/>
				<Route path = '/admins/trainers' component = {TrainersInformations}/>
				<Route path = '/admins/schedule' component = {AdminSchedule}/>
				<Route path = '/admins' component = {Admins}/>

				{/* trainers paths */}
				<Route path = '/trainers/schedule' component = {TrainerSchedule}/>
				<Route path = '/trainers/reservations' component = {ViewReservations}/>
				<Route path = '/trainers/users' component = {UsersDetails}/>
				<Route path = '/trainers/myAccount' component = {TrainerAccountInformations}/>
				<Route path = '/trainers/chat' component = {TrainerChat}/>
				<Route path = '/trainers' component = {Trainers}/>

			</Switch>
  		</main>	
		)
	}
}

ReactDOM.render(
	<BrowserRouter>
		<App />
	</BrowserRouter>,
	document.getElementById('root')
)
