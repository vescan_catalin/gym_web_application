import React from 'react'
import Input from '../input_components/Input'
import * as Actions from '../../actions/Actions'
import './PurchaseComponent.css'
import Button from '../button_component/Button'
import SubscriptionSelection from './SubscriptionSelection'

export default class PurchaseComponent extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            nameOnCard: 'Name on card',
            nameOnCardType: 'text',
            name: '',

            cardNo: 'Card number',
            carNoType: 'text',
            cardNr: '',

            expDate: 'Expiration date',
            expDateType: 'text',
            exp: '',

            CVC: 'CVC (3 numbers)',
            CVCType: 'password',
            cvc: '',

            amount: '',
            
        }
        this.onChange = this.onChange.bind(this)
        this.onClick = this.onClick.bind(this)
    }
    
    onChange(e) {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

    async onClick(e) {
        e.preventDefault()

        let user = window.localStorage.getItem('user')
        let subscription = document.getElementById('subscriptionChosed').value
        let name = this.state.name
        let cardNumber = this.state.cardNr
        let expiration = this.state.exp
        let cvc = this.state.cvc
        let amount = document.getElementById('amount').value.split(' ')[0] + '00'   // 00 for convertion and minimal amount...
        let currency = 'ron'


        if(name !== '' && cardNumber !== '' && expiration !== '' && cvc !== '') {
            let expirationMonth = expiration.split('/')[0]
            let expirationYear = expiration.split('/')[1]

            let customerDetails = {
                user,
                subscription,
                name,
                cardNumber,
                expirationMonth,
                expirationYear,
                cvc, 
                amount,
                currency
            }

            await Actions.pay(customerDetails).then(response => {
                if(response !== null) {
                    alert('Your transaction was successfully made!')
                } else {
                    alert('Something went wrong!')
                }
            })
        } else {
            alert('Please complete all the fields!')
        }

        
    }

    render() {
        return(
            <div>
                <form className = 'PurchaseForm'>
                    <Input className = 'PurchaseInput'
                        type = {this.state.nameOnCardType} 
                        labelName = {this.state.nameOnCard}
                        name = 'name'
                        value = {this.state.name}
                        onChange = {this.onChange}
                    /> <br />

                    <Input className = 'PurchaseInput'
                        type = {this.state.carNoType} 
                        labelName = {this.state.cardNo}
                        name = 'cardNr'
                        value = {this.state.cardNr}
                        onChange = {this.onChange}
                    /> <br />
                    
                    <Input className = 'PurchaseInput'
                        type = {this.state.expDateType} 
                        placeholder = 'MM/YY'
                        labelName = {this.state.expDate}
                        name = 'exp'
                        value = {this.state.exp}
                        onChange = {this.onChange}
                    /> <br />
                    
                    <Input className = 'CVC'
                        type = {this.state.CVCType} 
                        labelName = {this.state.CVC}
                        name = 'cvc'
                        value = {this.state.cvc}
                        onChange = {this.onChange}
                    />

                    <div className = 'ButtonPurchase'>
                        <Button 
                            name = 'Purchase'
                            onClick = {this.onClick}
                        />
                    </div>
                </form>
                
                <div className = 'Subscription'>
                    <SubscriptionSelection/>
                </div>
            </div>
        )
    }
}