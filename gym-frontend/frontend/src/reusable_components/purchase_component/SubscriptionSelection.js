import React from 'react'
import openSocket from 'socket.io-client'

import './PurchaseComponent.css'
import Input from '../input_components/Input';

var socket = openSocket('localhost:2999')

export default class SubscriptionSelection extends React.Component {
    constructor(props) {
        super(props)
        
        this.state = {
            description: 'Description',
            descriptionType: 'text',
            descriptionValue: '',
            price: 'Price',
            priceType: 'text',
            priceValue: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onChange() {

    }

    async handleChange(event) {
        await this.setState({value: event.target.value});

        socket.emit('subscriptionChosed', this.state.value)

        socket.on('subscriptionResponse', response => {
            this.setState({
                descriptionValue: response.description,
                priceValue: response.price + ' RON'
            })
        })
    }

    render() {
        return(
            <div>
                <select onChange = {this.handleChange} id  = 'subscriptionChosed'>
                    <option>1 day pass</option>
                    <option>silver</option>
                    <option>gold</option>
                    <option>platinum</option>
                    <option>students</option>
                    <option>family</option>
                    <option>1 year pass</option>
                </select>
            
                <Input className = 'DescriptionInput'
                    type = {this.state.descriptionType}
                    labelName = {this.state.description}
                    value = {this.state.descriptionValue}
                    onChange = {this.onChange}
                /> <br/>

                <Input className = 'PriceInput'
                    id = 'amount'
                    type = {this.state.priceType}
                    labelName = {this.state.price}
                    value = {this.state.priceValue}
                    onChange = {this.onChange}
                /> 
            </div>
        )
    }
}