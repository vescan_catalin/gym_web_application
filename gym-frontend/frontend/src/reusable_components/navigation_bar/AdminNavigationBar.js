import React from "react";
import * as NameConstants from "../../constants/NameConstants";
import * as AdminPathConstants from "../../constants/AdminPathConstants";
import WrappedLink from "../wrapped_link/WrappedLink";

export default class AdminNavigationBar extends React.Component {
    render() {
        return(
            <div className = "Container">
                <WrappedLink to = {AdminPathConstants.homePath}
                             name = {NameConstants.home}
                />
                <WrappedLink to = {AdminPathConstants.schedulePath}
                             name = {NameConstants.schedule}
                />
                <WrappedLink to = {AdminPathConstants.trainersPath}
                             name = {NameConstants.trainers}
                />
                <WrappedLink to = {AdminPathConstants.usersPath}
                             name = {NameConstants.users}
                />
                <WrappedLink to = {AdminPathConstants.subscriptionPath}
                             name = {NameConstants.subscription}
                />
                <WrappedLink to = {AdminPathConstants.productsPath}
                             name = {NameConstants.products}
                />
                <WrappedLink to = {AdminPathConstants.notificationsPath}
                             name = {NameConstants.notifications}
                />
                <WrappedLink to = {AdminPathConstants.chatPath}
                             name = {NameConstants.chat}
                />
                <WrappedLink to = {AdminPathConstants.logoutPath}
                             name = {NameConstants.logout}
                />
            </div> 
        );
    }
}