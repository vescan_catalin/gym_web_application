import React from "react";
import WrappedLink from "../wrapped_link/WrappedLink";
import * as NameConstants from "../../constants/NameConstants";
import * as UserPathConstants from "../../constants/UserPathConstants";
import "./NavigationBar.css";

export default class UserNavigationBar extends React.Component {
    render() {
        return(
            <div className = "Container">
                <WrappedLink to = {UserPathConstants.homePath}
                             name = {NameConstants.home}
                />
                <WrappedLink to = {UserPathConstants.schedulePath}
                             name = {NameConstants.schedule}
                />
                <WrappedLink to = {UserPathConstants.viewTrainersPath}
                             name = {NameConstants.trainers}
                />
                <WrappedLink to = {UserPathConstants.reservationsPath}
                             name = {NameConstants.reservations}
                />
                <WrappedLink to = {UserPathConstants.purchase}
                             name = {NameConstants.purchase}
                />      
                <WrappedLink to = {UserPathConstants.myAccountPath}
                             name = {NameConstants.myAccount}
                />   
                <WrappedLink to = {UserPathConstants.logoutPath}
                             name = {NameConstants.logout}
                />
            </div>
        );
    }
}