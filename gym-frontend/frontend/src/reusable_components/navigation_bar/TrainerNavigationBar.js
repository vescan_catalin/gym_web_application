import React from "react";
import WrappedLink from "../wrapped_link/WrappedLink";
import * as NameConstants from "../../constants/NameConstants";
import * as TrainerPathConstants from "../../constants/TrainerPathConstants";

export default class TrainerNavigationBar extends React.Component {
    render() {
        return(
            <div className = "Container">
                <WrappedLink to = {TrainerPathConstants.homePath}
                             name = {NameConstants.home}
                />
                <WrappedLink to = {TrainerPathConstants.schedulePath}
                             name = {NameConstants.schedule}
                />
                <WrappedLink to = {TrainerPathConstants.reservations}
                             name = {NameConstants.reservations}
                />
                <WrappedLink to = {TrainerPathConstants.usersPath}
                             name = {NameConstants.users}
                />
                <WrappedLink to = {TrainerPathConstants.myAccountPath}
                             name = {NameConstants.myAccount}
                />
                <WrappedLink to = {TrainerPathConstants.chatPath}
                             name = {NameConstants.chat}
                />
                <WrappedLink to = {TrainerPathConstants.logoutPath}
                             name = {NameConstants.logout}
                />
            </div>
        );
    }
}