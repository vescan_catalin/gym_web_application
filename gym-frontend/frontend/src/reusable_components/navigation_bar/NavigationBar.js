import React from "react";
import WrappedLink from "../wrapped_link/WrappedLink";
import * as NameConstants from "../../constants/NameConstants";
import * as HomePathConstants from "../../constants/HomePathConstants";
import "./NavigationBar.css";

export default class NavigationBar extends React.Component {
    render() {
        return(
            <div className = "Container">
                <WrappedLink to = {HomePathConstants.homePath}
                             name = {NameConstants.home}
                />
                <WrappedLink to = {HomePathConstants.schedulePath}
                             name = {NameConstants.schedule}
                />
                <WrappedLink to = {HomePathConstants.subscriptionPath}
                             name = {NameConstants.subscription}
                />
                <WrappedLink to = {HomePathConstants.findUsPath}
                             name = {NameConstants.findUs}
                />
                <WrappedLink to = {HomePathConstants.chatPath}
                             name = {NameConstants.chat}
                />
                <WrappedLink to = {HomePathConstants.loginPath}
                             name = {NameConstants.login}
                />
                <WrappedLink to = {HomePathConstants.viewTrainersPath}
                             name = {NameConstants.trainers}
                />
                <WrappedLink to = {HomePathConstants.photosPath}
                             name = {NameConstants.photos}
                />
                <WrappedLink to = {HomePathConstants.productsPath}
                             name = {NameConstants.products}
                />
            </div>
        );
    }
}