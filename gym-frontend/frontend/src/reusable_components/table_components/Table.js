import React from "react";
import Cell from "./Cell";
import "./Table.css";

export default class Table extends React.Component {    
    renderHeadingRow = (_cell, cellIndex) => {
        const {headings} = this.props;
        
        return(
            <Cell
                key = {`heading-${cellIndex}`}
                content = {headings[cellIndex].split("_")[0]}   // take the content
                header = {true}
                adminRole = {this.props.adminRole}
                id = {headings[cellIndex].split("_")[1]}        // take the db id
                buttonName = {this.props.buttonName}
                buttonKey = {this.props.buttonKey}
                editable = {this.props.editable}
                value = {this.props.value}
                onClick = {this.props.onClick}
                onChange = {this.props.onChange}
            />
        )
    };

    renderRow = (_row, rowIndex) => {
        const {rows} = this.props;

        return(
            <tr key={`row-${rowIndex}`}>
                {rows[rowIndex].map((_cell, cellIndex) => {
                    return(
                        <Cell
                            key = {`${rowIndex}-${cellIndex}`}
                            content = {rows[rowIndex][cellIndex].split("_")[0]} // take the content
                            adminRole = {this.props.adminRole}
                            id = {rows[rowIndex][cellIndex].split("_")[1]}      // take the db id
                            buttonName = {this.props.buttonName}
                            buttonKey = {this.props.buttonKey}
                            editable = {this.props.editable}
                            value = {this.props.value}
                            onClick = {this.props.onClick}
                            onChange = {this.props.onChange}
                        />
                    );
                })}
            </tr>
        );
    };

    render() {
        const {headings, rows} = this.props;
        
        this.renderHeadingRow = this.renderHeadingRow.bind(this);
        this.renderRow = this.renderRow.bind(this);

        const theadMarkup = (
            <tr key = "heading">
                {headings.map(this.renderHeadingRow)}
            </tr>
        );

        const tbodyMarkup = rows.map(this.renderRow);

        return(
            <table className = "Table">
                <thead> {theadMarkup}</thead>
                <tbody> {tbodyMarkup} </tbody>
            </table>
        );
    }
}