import React from "react";
import "./Cell.css";
import CellInput from "../input_components/CellInput";

export default class Cell extends React.Component {   
    render() {
        const {id, value, editable} = this.props
        const cellMarkup = this.props.header ? (
            <th className = "Cell"
                id = {this.props.id}
            >
                {this.props.adminRole && editable && this.props.id === this.props.buttonKey ?
                    <div>
                        <CellInput
                            editable = {this.props.editable}
                            value = {this.props.value === '' ? 
                                this.props.content : this.props.value
                            }
                            onChange = {this.props.onChange}
                        /> 
                    </div>
                    : 
                    <div>
                        {this.props.content}
                    </div>
                }
            </th>
        ) : (
            <td className = "Cell"
                id = {this.props.id}
            >
                {this.props.adminRole && editable && this.props.id === this.props.buttonKey ?
                    <div>
                        <CellInput
                            editable = {this.props.editable}
                            value = {this.props.value === '' ? 
                                this.props.content : this.props.value
                            }
                            buttonId = {this.props.id}
                            buttonKey = {this.props.buttonKey}
                            onChange = {this.props.onChange}
                        /> 
                        {editable ?
                            <button className = "TableButton"
                                    data-key = {this.props.id}
                                    onClick = {(e) => this.props.onClick(e, id, 
                                        value === '' ? this.props.content : this.props.value)
                                    }
                            >
                                {'save'}
                            </button> : null
                        }
                    </div>
                    : 
                    <div>
                        {this.props.content}
                        {this.props.adminRole && !editable ?
                            <button className = "TableButton"
                                    data-key = {this.props.id}
                                    onClick = {(e) => this.props.onClick(e, id)}
                            >
                                {'edit'}
                            </button> : null
                        }
                        
                    </div>
                }
            </td>
        );
    
        return (cellMarkup);
    }
}

