import React from "react"
import "./CellInput.css"

export default function CellInput({editable, value, onChange}) {
    const inputField = editable ? 
        <input 
            className = "CellInput"
            type = "text"
            name = "textInput" 
            value = {value}
            onChange = {onChange}
        /> : null
    
    return (inputField)
}