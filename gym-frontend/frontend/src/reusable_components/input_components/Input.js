import React from 'react';
import "./Input.css";

export default class Input extends React.Component {	
	render() {
		return(
			<div>
			<label className = "LabelText">{this.props.labelName}</label>
				<input className = {this.props.className}
						id = {this.props.id}
						type = {this.props.type} 
						placeholder = {this.props.placeholder}
						value = {this.props.value} 
						name = {this.props.name} 
						onChange = {this.props.onChange}
				/>
			</div>
		);
	}
}
