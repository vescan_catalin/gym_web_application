import React from 'react'

import MessageList from './MessageList'

export default class Chat extends React.Component {
  render() {
    return(
      <div>
        {this.props.navigationBar}
        <MessageList
          senderId = {this.props.senderId}
        />
      </div>
    )
  }
}