import React from 'react'
 
import './Chat.css'

export default class ChatWindow extends React.Component {


    render() {
        return (
            <div className = 'MessageWindow'>
                <textarea 
                    placeholder = ''
                    readOnly = {true}    
                >
                </textarea>
                <div className = 'InputField'>
                    <input
                        placeholder = 'Type your text here...'
                        type = 'text'
                        value = {this.props.message}
                        onChange = {this.props.onChange}
                        onKeyDown = {this.props.onKeyDown}
                    />
                </div>
            </div>
        )
    }
}