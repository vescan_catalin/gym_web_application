import React from 'react'
import openSocket from 'socket.io-client'

import './Chat.css'
import * as Actions from '../../actions/Actions'

var socket = openSocket('localhost:2999')

export default class MessageList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            conversation: {
                senderId: null,
                senderMessage: null
            },
            message: ''
        }
        this.onChange = this.onChange.bind(this)
        this.onKeyDown = this.onKeyDown.bind(this)
    }

    // load conversations from db
    async componentWillMount() {
        let chat = await Actions.getAllMessages()
        appendText(chat)
    }

    componentDidMount() {
        // receive conversation from db
        var textarea = document.getElementById('textarea')
        socket.on('event', data => {
            textarea.value += data.sender + ': ' + data.message + '\n'
            textarea.scrollTop = textarea.scrollHeight
        })
        socket.on('acknowledge', chatIsClear => {
            if(chatIsClear) 
                textarea.value = ''
        })
    }

    onChange(e) {
        this.setState({
            message: e.target.value,
        })
    }

    // when i press enter this method will be executed
    async onKeyDown(e) {
        if(e.key === 'Enter' && this.state.message !== '') {
            await this.setState({
                conversation: {
                    senderId: this.props.senderId,
                    senderMessage: this.state.message
                }  
            })

            let offensiveWords = await Actions.getOffensiveWords()
            let message = this.state.conversation.senderMessage

            if(messageIsClear(message, offensiveWords)) {
                // here i send message with sender owner to db
                let data = {
                    sender: this.state.conversation.senderId,
                    message: this.state.conversation.senderMessage
                }
                
                // send conversation to server
                socket.emit('message', data)
            } else {
                alert('Your message contains inapropriate content')
            }
            
            // clear input field after message was sent
            document.getElementById('input').value = ''
            this.setState({
                message: ''
            })
        }   
        
    }

    render() {
        return (
            <div className = 'ParentWindow'>
                <div className = 'MessageWindow'>
                    <textarea 
                        id = 'textarea'
                        placeholder = ''
                        readOnly = {true}
                    />
                    <div className = 'InputField'>
                        <input
                            id = 'input'
                            placeholder = 'Type your text here...'
                            type = 'text'
                            value = {this.state.message}
                            onChange = {this.onChange}
                            onKeyDown = {this.onKeyDown}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

function messageIsClear(message, restrictiveWords) {
    var clearMessage = true

    restrictiveWords.forEach(word => {
        if(message.includes(word)) {
            clearMessage = false
            return
        }
    })

    return clearMessage
}

function appendText(chat) {
    chat.map((conversation, i) => {
        return (
            document.getElementById('textarea').value += 
            conversation.sender + ': ' + conversation.message + '\n'
        )
    })
}
