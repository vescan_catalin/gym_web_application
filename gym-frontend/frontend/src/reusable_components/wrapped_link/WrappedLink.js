import React from "react";
import {Link} from "react-router-dom";
import "./WrappedLink.css";

export default class WrappedLink extends React.Component {
  render() {  
    return (
        <Link className = "Link"
              to = {this.props.to}
        >
              {this.props.name}
        </Link>
      )
    }
  }