import React from 'react'
// import {Link, withRouter} from 'react-router-dom'
import Input from '../../reusable_components/input_components/Input'
import Button from '../../reusable_components/button_component/Button'

import './Login.css'
import * as Actions from '../../actions/Actions'

export default class Login extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			username: 'Username :',
			password: 'Password :',
			login: 'Login',
			register: 'Register',
			userType: 'text',
			passType: 'password',
			userText: '',
			userPass: '',
			type: 'user',
			userExists: false,
			loginRedirect: '',
			registerRedirect: '',
			logSuccess: false,
			regSuccess: false,
			userReceived: null,
			passwordRegex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[,.:;\-_?!@#%&*+])\S{8,12}$/
		}
		this.onChange = this.onChange.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
		this.register = this.register.bind(this)
	}

	componentWillMount() {
		window.localStorage.clear()
	}
	
	onChange(e) {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

	async onSubmit(e) {
		e.preventDefault()
		let user = {
			name: this.state.userText,
			password: this.state.userPass
		}

		await Actions.login(user).then(data => {
			if(data.status !== 500) {
				this.setState({
				userReceived: data,
				logSuccess: true
				})

				window.localStorage.setItem('user', data.name)
				window.localStorage.setItem('userType', data.type)
			} else {
				alert('Username or password was wrong !')
			}
		})
		
		if(this.state.logSuccess) {
			alert('Successfully logged-in!')

			if(this.state.userReceived.type === 'user') {
				this.props.history.push('/users')
			}
			if(this.state.userReceived.type === 'trainer') {
				this.props.history.push('/trainers')
			}
			if(this.state.userReceived.type === 'admin') {
				this.props.history.push('/admins')
			}
		}
		

	}	

	async register(e) {
		e.preventDefault()

		// get user data from interface
		let user = {
			name: this.state.userText,
			password: this.state.userPass,
			type: this.state.type
		}

		let passwordValidation = this.state.passwordRegex.test(this.state.userPass)

		if(passwordValidation) {
			await Actions.register(user).then(data => {
				if(data.status !== 500) {

					this.setState({
						user: data,
						regSuccess: true
					})
					
					if(this.state.regSuccess) {
						alert('Successfully registered!')
					}
				} else {
					alert('Please chose another username!')
				}
			})
		} else {
			alert('Please chose a stronger password!')
		}

		
	}

	render() {
		return(
			<div >
				<form>
					<Input 
							type = {this.state.userType} 
							labelName = {this.state.username}
							name = 'userText'
							value = {this.state.userText}
							onChange = {this.onChange}/> <br />
					<Input 
							type = {this.state.passType}
							labelName = {this.state.password}
							name = 'userPass'
							value = {this.state.userPass}
							onChange = {this.onChange}/> <br />
					
					<Button onClick = {this.onSubmit}
							name = {this.state.login}/>

					<Button onClick = {this.register} 
							name = {this.state.register} /> <br />
				</form>
			</div>
		)
	}
}
