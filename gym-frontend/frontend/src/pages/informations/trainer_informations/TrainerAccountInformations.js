import React from 'react'
import TrainerNavigationBar from '../../../reusable_components/navigation_bar/TrainerNavigationBar'
import Input from '../../../reusable_components/input_components/Input'
import Button from '../../../reusable_components/button_component/Button'

import './TrainerAccountInformations.css'

export default class TrainerAccountInformations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            trainerType: 'text',
            trainerName: 'Username: ',
            trainerText: '',
            trainerPass: 'password',
            oldPass: 'Old password: ',
            oldPassword: '',
            newPass: 'New Password: ',
            newPassword: '',
            setEmail: 'E-mail: ',
            trainerEmail: '',
            save: 'Save',
            type: 'file',
            updatedtrainer: null
        }

        this.onChange = this.onChange.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
    }

    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }

    onChange(e) {
		this.setState({
			[e.target.name]: e.target.value
		})
	}

    onSubmit(e) {
        e.preventDefault()
    }

    render() {
        return(
            <div>
                <TrainerNavigationBar/>

                <div className = 'ContainerAccount'>
                    <form className = 'FormAccount'>
                    <Input 
                                className = 'InputFixed'
                                type = {this.state.trainerType}
                                labelName = {this.state.trainerName}
                                name = 'trainerText'
                                value = {this.state.trainerText}
                                onChange = {this.onChange}
                        /> <br/>
                        <Input 
                                className = 'InputFixed'
                                type = {this.state.trainerPass}
                                labelName = {this.state.oldPass}
                                name = 'oldPassword'
                                value = {this.state.oldPassword}
                                onChange = {this.onChange}
                        /> <br/>
                        <Input 
                                className = 'InputFixed'
                                type = {this.state.trainerPass}
                                labelName = {this.state.newPass}
                                name = 'newPassword'
                                value = {this.state.newPassword}
                                onChange = {this.onChange}
                        /> <br/>
                        <Input 
                                className = 'InputFixed'
                                type = {this.state.trainerType}
                                labelName = {this.state.setEmail}
                                name = 'trainerEmail'
                                value = {this.state.trainerEmail}
                                onChange = {this.onChange}
                        /> <br/>

                        <Button 
                                onClick = {this.onSubmit}
                                name = {this.state.save}
                        /> <br/>                        
                    </form>

                </div>
            </div>
        )
    }
}