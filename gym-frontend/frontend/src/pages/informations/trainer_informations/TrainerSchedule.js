import React from 'react'
import Table from '../../../reusable_components/table_components/Table'
import TrainerNavigationBar from '../../../reusable_components/navigation_bar/TrainerNavigationBar'
import * as Actions from '../../../actions/Actions'
import './TrainerSchedule.css'

export default class TrainerSchedule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: [],
            rows: [],
            buttonName: 'edit',
            save: false,
            editable: false,
            adminRole: true,
            textInput: '',
            cellId: 0,
            buttonKey: 0,
        }
        this.onClick = this.onClick.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
		this.setState({
			[e.target.name]: e.target.value
        });
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }

        let hd = []
        let allRows = []

        await Actions.loadSchedule().then(data => {
            for(let i = 0; i < 8; i++) {
                hd.push(data[i].text + '_' + data[i].id)
            }
    
            for(let i = 8; i < data.length; ) {
                let eachRow = []
                for(let j = 0; j < 8; j++) {
                    eachRow.push(data[i].text + '_' + data[i].id)
                    i++
                }
                allRows.push(eachRow)
            }
            
            this.setState({
                headings: hd,
                rows: allRows,
            })
        })

    }

    // save data in database
    async onSubmit(val) {
        let cellSchedule = {
            id: this.state.cellId,
            text: val
        }

        if(this.state.save === true) {
            await Actions.updateSchedule(cellSchedule)
            
            // reload the current page
            window.location.reload()
        }
        
    }

    async onClick(e, key, val)  {
        // button id ( is the same as cell id )
        await this.setState({
            buttonKey: e.target.getAttribute('data-key')
        })

        // console.log(this.state.buttonKey === key)
        
        if(this.state.buttonName === 'edit') {
            // edit mode
            await this.setState({
                buttonName: 'save',
                save: false,
                editable: true,
            })
        } else {
            // save mode
            await this.setState({
                buttonName: 'edit',
                save: true,
                editable: false,
                cellId: key
            })
            
            // submit changes
            this.onSubmit(val)
        }
    }
    
    render() {
        return(
            <div className = 'ScheduleContainer'>
                
                <TrainerNavigationBar/>
                
                <div className = 'TableContainer'>
                    <Table                         
                        headings = {this.state.headings} 
                        rows = {this.state.rows}
                        buttonName = {this.state.buttonName}
                        buttonKey = {this.state.buttonKey}
                        editable = {this.state.editable}
                        adminRole = {this.state.adminRole}
                        value = {this.state.textInput}
                        onClick = {this.onClick} 
                        onChange = {this.onChange}
                    />
                </div>

            </div>
        )
    }
}