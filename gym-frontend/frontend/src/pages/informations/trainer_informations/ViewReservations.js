import React from "react"
import TrainerNavigationBar from '../../../reusable_components/navigation_bar/TrainerNavigationBar'
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table'
import './ViewReservations.css'

export default class ViewReservations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Username', 'Class', 'Reservation Date'],
            rows: [],
            adminRole: false
        }
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }

        let allRows = []

        await Actions.getReservations().then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push(data[i].username)
                rws.push(data[i].class)
                rws.push(data[i].date)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                <TrainerNavigationBar/>

                <div className = 'ReservationsTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>

            </div>
        );
    }
}