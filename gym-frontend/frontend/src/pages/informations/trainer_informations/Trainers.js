import React from "react"
import TrainerNavigationBar from '../../../reusable_components/navigation_bar/TrainerNavigationBar'

import './Trainers.css'

export default class Trainers extends React.Component {
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }

    render() {
        return(
            <div>
                <TrainerNavigationBar/>

                <div className = 'TrainerTitle'>
                    <h1>
                        Bun venit, {window.localStorage.getItem('user')} !
                    </h1>
                </div>

                <div className = 'TrainerText'>
                    <p>
                        &emsp;&emsp;„Sportul reprezintă orice formă de activitate fizică ce are ca obiectiv exprimarea sau 
                        imbunătățirea condiției fizice și psihice, dezvoltarea relațiilor sociale sau 
                        obținerea unor rezultate în competiții la toate nivelele, prin intermediul unei 
                        activități organizate sau nu.”
                    </p>
                    <p>
                        Cu alte cuvinte, sportul înseamnă sănătate!
                    </p>
                </div>
            </div>
        );
    }
}