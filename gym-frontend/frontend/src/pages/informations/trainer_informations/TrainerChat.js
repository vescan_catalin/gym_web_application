import React from 'react'
import Chat from '../../../reusable_components/chat/Chat';
import TrainerNavigationBar from '../../../reusable_components/navigation_bar/TrainerNavigationBar';

export default class TrainerChat extends React.Component {
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }
    
    render() {
        return (
            <Chat
                navigationBar = {<TrainerNavigationBar/>}
                senderId = 'MyGym'
            />
        )
    }
}