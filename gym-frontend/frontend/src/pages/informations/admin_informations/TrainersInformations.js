import React from 'react'
import AdminNavigationBar from '../../../reusable_components/navigation_bar/AdminNavigationBar';
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table'
import './TrainersInformations.css'

export default class TrainersInformations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Id', 'Username', 'User type', 'Email'],
            rows: [],
            adminRole: true,
            userType: 'trainer'
        }
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'trainer'){
            this.props.history.push('/trainers')
        }

        let allRows = []

        await Actions.getUsersByType(this.state.userType).then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push('' + data[i].id)
                rws.push(data[i].name)
                rws.push(data[i].type)
                rws.push(data[i].email === null ? '' : data[i].email)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                <AdminNavigationBar/>

                <div className = 'TrainersInformationsTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        )
    }
}