import React from 'react'
import AdminNavigationBar from '../../../reusable_components/navigation_bar/AdminNavigationBar'
import Button from '../../../reusable_components/button_component/Button'
import * as Actions from '../../../actions/Actions'

import './Notifications.css'
import './AdminSchedule.css'

export default class Notifications extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            send: 'Send',
            defaultValue: 'Type your message here...',
            finalValue: '',
            autoFocus: true
        }

        this.onChange = this.onChange.bind(this)
        this.onClick = this.onClick.bind(this)
    }

    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'trainer'){
            this.props.history.push('/trainers')
        }
    }

    onChange(e) {
        this.setState({
            finalValue: e.target.value
         });
    }

    async onClick(e) {
        let users = await Actions.getUsers()

        await Actions.sendEmail(users, this.state.finalValue)
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                
                <AdminNavigationBar/>
                
                <div className = 'Label'>
                    <h1>Introdu mesajul pe care vrei sa-l transmiti utilizatorilor.</h1>
                
                    <div>
                        <textarea
                                className = 'TextArea'
                                ref = 'textArea'
                                autoFocus = {this.state.autoFocus}
                                placeholder = {this.state.defaultValue}
                                onChange = {this.onChange}
                        />
                    </div>

                    <div className = 'Send'>
                        <Button onClick = {this.onClick}
                                name = {this.state.send}
                        />
                    </div>
                
                </div>
                
            </div>
        )
    }
}