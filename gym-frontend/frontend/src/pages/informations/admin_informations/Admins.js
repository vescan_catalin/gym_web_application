import React from "react";
import AdminNavigationBar from "../../../reusable_components/navigation_bar/AdminNavigationBar";

import './Admins.css'

export default class Admins extends React.Component {

    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'trainer'){
            this.props.history.push('/trainers')
        }
    }

    render() {
        return(
            <div>
                <AdminNavigationBar/>

                <div className = 'AdminTitle'>
                    <h1>
                        Bun venit, {window.localStorage.getItem('user')} !
                    </h1>
                </div>

                <div className = 'AdminText'>
                    <p>
                        &emsp;&emsp;„Sportul reprezintă orice formă de activitate fizică ce are ca obiectiv exprimarea sau 
                        imbunătățirea condiției fizice și psihice, dezvoltarea relațiilor sociale sau 
                        obținerea unor rezultate în competiții la toate nivelele, prin intermediul unei 
                        activități organizate sau nu.”
                    </p>
                    <p>
                        Cu alte cuvinte, sportul înseamnă sănătate!
                    </p>
                </div>
            </div>
        );
    }
}