import React from 'react'
import Chat from '../../../reusable_components/chat/Chat';
import AdminNavigationBar from '../../../reusable_components/navigation_bar/AdminNavigationBar';
import Button from '../../../reusable_components/button_component/Button';
// import * as Actions from '../../../actions/Actions'
import './AdminChat.css'
import openSocket from 'socket.io-client'

var socket = openSocket('localhost:2999')

export default class AdminChat extends React.Component {
    constructor(props) {
        super(props)

        this.onClick = this.onClick.bind(this)
    }

    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'trainer'){
            this.props.history.push('/trainers')
        }
    }

    onClick() {
        // clear chat when button is pressed
        socket.emit('delete')
    }
    
    render() {
        return (
            <div className = 'CoverDiv'>
                <Chat
                    navigationBar = {<AdminNavigationBar/>}
                    senderId = 'MyGym'
                />

                <div className = 'ClearChatButton'>
                    <Button
                        onClick = {this.onClick}
                        name = 'clear chat'
                    />
                </div>
            </div>
        )
    }
}