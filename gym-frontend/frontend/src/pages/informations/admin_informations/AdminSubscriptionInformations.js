import React from 'react'

import AdminNavigationBar from '../../../reusable_components/navigation_bar/AdminNavigationBar'
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table';

import './AdminSubscriptionInformations.css'

export default class AdminSubscriptionInformations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Type', 'Description', 'Price'],
            rows: [],
            buttonName: 'edit',
            save: false,
            editable: false,
            adminRole: true,
            textInput: '',
            cellId: 0,
            buttonKey: 0
        }
        this.onClick = this.onClick.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'trainer'){
            this.props.history.push('/trainers')
        }
        
        let allRows = []
        let eachCellId = 1

        await Actions.getAllSubscriptions().then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push(data[i].type + '_' + eachCellId++)
                rws.push(data[i].description + '_' + eachCellId++)
                rws.push('' + data[i].price + '_' + eachCellId++)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }

    onChange(e) {
		this.setState({
			[e.target.name]: e.target.value
        });
    }

    async onClick(e, key, val)  {
        // button id ( is the same as cell id )
        await this.setState({
            buttonKey: e.target.getAttribute('data-key')
        })
        
        if(this.state.buttonName === 'edit') {
            // edit mode
            await this.setState({
                buttonName: 'save',
                save: false,
                editable: true,
            })
        } else {
            // save mode
            await this.setState({
                buttonName: 'edit',
                save: true,
                editable: false,
                cellId: key
            })
            
            // submit changes
            this.onSubmit(val)
        }
    }

    // save data in database
    async onSubmit(val) {
        let subscription

        let clickedCellId = parseInt(this.state.cellId)

        if((clickedCellId + 3) % 3 === 1) {        // type
            subscription = {
                id: (clickedCellId + 2) / 3,
                type: this.state.textInput
            }
        } else if((clickedCellId + 3) % 3 === 2) { // description
            subscription = {
                id: (clickedCellId + 2) / 3,
                description: this.state.textInput
            } 
        } else if(clickedCellId % 3 === 0) {     // price
            subscription = {
                id: (clickedCellId + 2) / 3,
                price: this.state.textInput
            }
        }

        if(this.state.save === true) {
            await Actions.updateSubscription(subscription)
            
            // reload the current page
            window.location.reload()
        }
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                
                <AdminNavigationBar/>

                <div className = 'SubscriptionTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        buttonName = {this.state.buttonName}
                        buttonKey = {this.state.buttonKey}
                        editable = {this.state.editable}
                        adminRole = {this.state.adminRole}
                        value = {this.state.textInput}
                        onClick = {this.onClick} 
                        onChange = {this.onChange}
                    />
                </div>
            </div>
        );
    }
}