import React from 'react'
import AdminNavigationBar from '../../../reusable_components/navigation_bar/AdminNavigationBar';
import './ProductsInformations.css'

export default class ProductsInformations extends React.Component {
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'user') {
            this.props.history.push('/users')
        } else if(userType === 'trainer'){
            this.props.history.push('/trainers')
        }
    }

    render() {
        return(
            <div>
                <AdminNavigationBar/>

                <div className = 'Title'>
                    <h1> Produse MyGym </h1>
                </div>

                <div className = 'Text'>
                    <p>
                        &emsp;&emsp;În curând vom afișa lista cu produsele pe care dumneavoastră
                        le veți putea achiziționa din secțiunea corespunzătoare, 
                        ulterior autentificării. În caz că nu aveți un cont, vă
                        recomandăm călduros să vă înregistrați pentru a ne bucura
                        împreună de mai multe beneficii.
                    </p>
                    <p>
                        Mulțumim!
                    </p>
                </div>
            </div>
        )
    }
}