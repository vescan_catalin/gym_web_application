import React from "react";
import Table from "../../../reusable_components/table_components/Table";
import "./UserSchedule.css";
import UserNavigationBar from "../../../reusable_components/navigation_bar/UserNavigationBar";
import * as Actions from "../../../actions/Actions"

export default class UserSchedule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: [],
            rows: [],
            adminRole: false
        }
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
        
        let hd = []
        let allRows = []
        
        await Actions.loadSchedule().then(data => {
            console.log(data[0])
            for(let i = 0; i < 8; i++) {
                hd.push(data[i].text)
            }
    
            for(let i = 8; i < data.length; ) {
                let eachRow = []
                for(let j = 0; j < 8; j++) {
                    eachRow.push(data[i++].text)
                }
                allRows.push(eachRow)
            }
            
            this.setState({
                headings: hd,
                rows: allRows
            })
        })
    }

    render() {

        return(
            <div className = "ScheduleContainer">
                
                <UserNavigationBar/>
                
                <div className = "TableContainer">
                    <Table 
                        headings = {this.state.headings} 
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        );
    }
}