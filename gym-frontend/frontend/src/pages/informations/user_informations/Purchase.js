import React from 'react'
import UserNavigationBar from '../../../reusable_components/navigation_bar/UserNavigationBar';
import PurchaseComponent from '../../../reusable_components/purchase_component/PurchaseComponent';

import './Purchase.css'

export default class Purchase extends React.Component {
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }
    
    render() {
        return(
            <div>
                <UserNavigationBar/>

                <div className = 'Purchase'>
                    <PurchaseComponent/>
                </div>
            </div>
        )
    }
}