import React from "react";
import UserNavigationBar from "../../../reusable_components/navigation_bar/UserNavigationBar";
import Input from "../../../reusable_components/input_components/Input";
import Button from "../../../reusable_components/button_component/Button";
import * as Action from "../../../actions/Actions";

import "./MyAccount.css";

export default class MyAccount extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userType: "text",
            userName: "Username: ",
            userText: "",
            userPass: "password",
            oldPass: "Old password: ",
            oldPassword: "",
            newPass: "New Password: ",
            newPassword: "",
            setEmail: "E-mail: ",
            userEmail: "",
            save: "Save",
            updatedUser: null
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }

	onChange(e) {
		this.setState({
			[e.target.name]: e.target.value
		});
	}

    async onSubmit(e) {
        e.preventDefault();

        let data = {
            name: this.state.userText,
            password: this.state.newPassword,
            email: this.state.userEmail
        }

        await Action.changeData(data).then(response => {
            this.setState({
                updatedUser: response
            })
        });
    }

    render() {
        return(
            <div>
                <UserNavigationBar/>

                <div className = "ContainerAccount">
                    <form className = "FormAccount">
                        <Input 
                                className = "InputFixed"
                                type = {this.state.userType}
                                labelName = {this.state.userName}
                                name = "userText"
                                value = {this.state.userText}
                                onChange = {this.onChange}
                        /> <br/>
                        <Input 
                                className = "InputFixed"
                                type = {this.state.userPass}
                                labelName = {this.state.oldPass}
                                name = "oldPassword"
                                value = {this.state.oldPassword}
                                onChange = {this.onChange}
                        /> <br/>
                        <Input 
                                className = "InputFixed"
                                type = {this.state.userPass}
                                labelName = {this.state.newPass}
                                name = "newPassword"
                                value = {this.state.newPassword}
                                onChange = {this.onChange}
                        /> <br/>
                        <Input 
                                className = "InputFixed"
                                type = {this.state.userType}
                                labelName = {this.state.setEmail}
                                name = "userEmail"
                                value = {this.state.userEmail}
                                onChange = {this.onChange}
                        /> <br/>
                        
                        <Button 
                                onClick = {this.onSubmit}
                                name = {this.state.save}
                        />
                    </form>
                </div>
            </div>

        );
    }
}