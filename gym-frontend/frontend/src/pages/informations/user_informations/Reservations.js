import React from 'react'
import UserNavigationBar from '../../../reusable_components/navigation_bar/UserNavigationBar';
import Calendar from 'react-calendar'
import * as Actions from '../../../actions/Actions'
import './Reservations.css'
import Input from '../../../reusable_components/input_components/Input';
import Button from '../../../reusable_components/button_component/Button';

export default class Reservations extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            date: '',

            hour: 'Hour',
            hourType: 'text',
            time: '',

            name: 'Class name',
            nameType: 'text',
            class: ''
        }

        this.onChange = this.onChange.bind(this)
        this.onClickDay = this.onClickDay.bind(this)
        this.onClick = this.onClick.bind(this)
    }

    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }

    onChange(e) {
        this.setState({
			[e.target.name]: e.target.value
		});
    }

    onClickDay(e) {
        this.setState({
            date: e.toString().split('00:')[0]
        })
    }

    async onClick() {
        let reservationDate = this.state.date
        let reservationTime = this.state.time
        let nameOfTheClass = this.state.class

        if(reservationDate !== '' && reservationTime !== '' && nameOfTheClass !== '') {
            let reservation = {
                class: nameOfTheClass,
                user: window.localStorage.getItem('user'),
                date: reservationDate + ' ' + reservationTime
            }

            await Actions.addReservation(reservation).then(result => {
                if(result === 'IM_USED') {
                    alert('Only one reservation for the same user at the same date and time')
                } else {
                    alert('Your reservation was successfully created!')
                }
            })
        } else {
            alert('Please complete all fields !')
        }
    }

    render() {
        return(
            <div>
                <UserNavigationBar/>

                <div className = 'Reservations'>
                    <div className = 'Calendar'>
                        <Calendar
                            onClickDay = {this.onClickDay}
                        />
                    </div>

                    <div className = 'InputDiv'>
                        <Input className = 'HourInput'
                            type = {this.state.hourType} 
                            labelName = {this.state.hour}
                            name = 'time'
                            placeholder = 'HH:MM'
                            value = {this.state.time}
                            onChange = {this.onChange}
                        /> <br />

                        <Input className = 'ClassNameInput'
                            type = {this.state.nameType} 
                            labelName = {this.state.name}
                            name = 'class'
                            value = {this.state.class}
                            onChange = {this.onChange}
                        /> <br />

                        <Button
                            name = 'Apply'
                            onClick = {this.onClick}
                        />
                    </div>
                </div>
            </div>
        )
    }
}