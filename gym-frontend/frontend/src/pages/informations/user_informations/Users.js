import React from "react"
import UserNavigationBar from '../../../reusable_components/navigation_bar/UserNavigationBar'

import './Users.css'

export default class Users extends React.Component {
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
    }

    render() {
        return(
           <div>
                <UserNavigationBar/>

                <div className = 'UserTitle'>
                    <h1>
                        Bun venit, {window.localStorage.getItem('user')} !
                    </h1>
                </div>

                <div className = 'UserText'>
                    <p>
                        &emsp;&emsp;„Sportul reprezintă orice formă de activitate fizică ce are ca obiectiv exprimarea sau 
                        imbunătățirea condiției fizice și psihice, dezvoltarea relațiilor sociale sau 
                        obținerea unor rezultate în competiții la toate nivelele, prin intermediul unei 
                        activități organizate sau nu.”
                    </p>
                    <p>
                        Cu alte cuvinte, sportul înseamnă sănătate!
                    </p>
                </div>
           </div>
        );
    }

}