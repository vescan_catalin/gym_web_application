import React from 'react'

import UserNavigationBar from '../../../reusable_components/navigation_bar/NavigationBar'
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table';

import './SubscriptionInformations.css'

export default class SubscriptionInformations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Type', 'Description', 'Price'],
            rows: [],
            adminRole: false
        }
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }
        
        let allRows = []

        await Actions.getAllSubscriptions().then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push(data[i].type)
                rws.push(data[i].description)
                rws.push('' + data[i].price)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                
                <UserNavigationBar/>

                <div className = 'SubscriptionTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        );
    }
}