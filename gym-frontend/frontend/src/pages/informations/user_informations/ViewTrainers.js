import React from 'react'
import UserNavigationBar from '../../../reusable_components/navigation_bar/UserNavigationBar'
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table'
import './ViewTrainers.css'

export default class ViewTrainers extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Id', 'Username', 'User type', 'Email'],
            rows: [],
            adminRole: true,
            userType: 'trainer'
        }
    }

    async componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(window.localStorage.length === 0) {
            this.props.history.push('/')
        }

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        }

        let allRows = []

        await Actions.getUsersByType(this.state.userType).then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push('' + data[i].id)
                rws.push(data[i].name)
                rws.push(data[i].type)
                rws.push(data[i].email === null ? '' : data[i].email)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }
    
    render() {
        return(
            <div  className = 'ScheduleContainer'>
                <UserNavigationBar/>

                <div className = 'TrainersInformationsTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        )
    }
}