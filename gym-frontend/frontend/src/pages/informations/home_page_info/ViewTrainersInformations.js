import React from 'react'
import NavigationBar from '../../../reusable_components/navigation_bar/NavigationBar'
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table'
import './ViewTrainersInformations.css'

export default class ViewTrainersInformations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Id', 'Username', 'User type', 'Email'],
            rows: [],
            adminRole: false,
            userType: 'trainer'
        }
    }

    async componentWillMount() {
        let allRows = []

        await Actions.getUsersByType(this.state.userType).then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push('' + data[i].id)
                rws.push(data[i].name)
                rws.push(data[i].type)
                rws.push(data[i].email === null ? '' : data[i].email)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                <NavigationBar/>

                <div className = 'TrainersInformationsTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        )
    }
}