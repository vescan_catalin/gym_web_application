import React from 'react'

import NavigationBar from '../../../reusable_components/navigation_bar/NavigationBar'
import * as Actions from '../../../actions/Actions'
import Table from '../../../reusable_components/table_components/Table';

import './SubscriptionInformations.css'

export default class SubscriptionInformations extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: ['Type', 'Description', 'Price'],
            rows: [],
            adminRole: false
        }
    }

    async componentWillMount() {
        let allRows = []

        await Actions.getAllSubscriptions().then(data => {
            for(let i = 0; i < data.length; i++) {
                let rws = []
                rws.push(data[i].type)
                rws.push(data[i].description)
                rws.push('' + data[i].price)

                allRows.push(rws)
            }

            this.setState({
                rows: allRows
            })
        })
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                
                <NavigationBar/>

                <div className = 'SubscriptionTableContainer'>
                    <Table
                        headings = {this.state.headings}
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        );
    }
}