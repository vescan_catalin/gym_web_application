import React from 'react';
import Table from '../../../reusable_components/table_components/Table';
import NavigationBar from '../../../reusable_components/navigation_bar/NavigationBar';
import './Schedule.css';
import * as Actions from '../../../actions/Actions'

export default class Schedule extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headings: [],
            rows: [],
            adminRole: false
        }
    }
    
    async componentWillMount() {
        let hd = []
        let allRows = []
        
        await Actions.loadSchedule().then(data => {
            for(let i = 0; i < 8; i++) {
                hd.push(data[i].text)
            }
    
            for(let i = 8; i < data.length; ) {
                let eachRow = []
                for(let j = 0; j < 8; j++) {
                    eachRow.push(data[i++].text)
                }
                allRows.push(eachRow)
            }
            
            this.setState({
                headings: hd,
                rows: allRows
            })
        })
    }

    render() {
        return(
            <div className = 'ScheduleContainer'>
                
                <NavigationBar/>
                
                <div className = 'TableContainer'>
                    <Table 
                        headings = {this.state.headings} 
                        rows = {this.state.rows}
                        adminRole = {this.state.adminRole}
                    />
                </div>
            </div>
        );
    }
}