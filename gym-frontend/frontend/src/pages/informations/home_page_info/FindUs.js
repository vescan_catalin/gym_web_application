import React from "react";
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';
import * as Coordinates from "../../../constants/MapCoordinates";

class FindUs extends React.Component {
    render() {
        return(
        <Map 
                google={this.props.google} 
                initialCenter={{
                    lat: Coordinates.lat,
                    lng: Coordinates.lng
                    }}
                zoom={14}
        >

            <Marker onClick={this.onMarkerClick}
                    name={'Current location'} 
            />

        </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: (Coordinates.key)
  })(FindUs)
