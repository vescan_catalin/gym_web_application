import React from "react"
import NavigationBar from "../../../reusable_components/navigation_bar/NavigationBar"
import './Products.css'

export default class Products extends React.Component {
    render() {
        return(
            <div>
                <NavigationBar/>

                <div className = 'Title'>
                    <h1> Produse MyGym </h1>
                </div>

                <div className = 'Text'>
                    <p>
                        &emsp;&emsp;În curând vom afișa lista cu produsele pe care dumneavoastră
                        le veți putea achiziționa din secțiunea corespunzătoare, 
                        ulterior autentificării. În caz că nu aveți un cont, vă
                        recomandăm călduros să vă înregistrați pentru a ne bucura
                        împreună de mai multe beneficii.
                    </p>
                    <p>
                        Mulțumim!
                    </p>
                </div>
            </div>
        );
    }
}