import React from "react";
import NavigationBar from "../../../reusable_components/navigation_bar/NavigationBar";

import './Home.css'

export default class Home extends React.Component {
    componentWillMount() {
        let userType = window.localStorage.getItem('userType')

        if(userType === 'trainer') {
            this.props.history.push('/trainers')
        } else if(userType === 'admin'){
            this.props.history.push('/admins')
        } else if(userType === 'user') {
            this.props.history.push('/users')
        }
    }

    render() {
        return(
            <div>
                <NavigationBar />  

                <div className = 'Title'>
                    <h1> MyGym vă urează bun venit ! </h1>
                </div>

                <div className = 'Text'>
                    <p>
                        &emsp;&emsp;„Sportul reprezintă orice formă de activitate fizică ce are ca obiectiv 
                        exprimarea sau imbunătățirea condiției fizice și psihice, dezvoltarea relațiilor sociale 
                        sau obținerea unor rezultate în competiții la toate nivelele, prin intermediul unei 
                        activități organizate sau nu.”
                    </p>
                    <p>
                        Cu alte cuvinte, sportul înseamnă sănătate!
                    </p>
                </div>
            </div>
        );
    }

}