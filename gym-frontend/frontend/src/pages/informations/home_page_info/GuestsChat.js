import React from 'react'

import Chat from '../../../reusable_components/chat/Chat'
import NavigationBar from '../../../reusable_components/navigation_bar/NavigationBar';

export default class GuestsChat extends React.Component {
    render() {
        return(
            <Chat 
                navigationBar = {<NavigationBar/>}
                senderId = 'Guest'
            />
        )
    }
}