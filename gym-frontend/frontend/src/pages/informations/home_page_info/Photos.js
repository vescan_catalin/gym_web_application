import React from "react"
import NavigationBar from "../../../reusable_components/navigation_bar/NavigationBar";
import './Photos.css'
import img1 from '../../../images/img1.jpg'
import img2 from '../../../images/img2.jpg'
import img3 from '../../../images/img3.jpg'
import img4 from '../../../images/img4.jpg'
import img5 from '../../../images/img5.jpg'
import img6 from '../../../images/img6.jpg'

export default class Photos extends React.Component {
    render() {
        return(
            <div>
                <NavigationBar/>

                <div className = 'Album'>
                    <div className = 'Img1'>
                        <img src = {img1}></img>
                    </div>
                    <div className = 'Img2'>
                        <img src = {img2}></img>
                    </div>
                    <div className = 'Img3'>
                        <img src = {img3}></img>
                    </div>

                    <div className = 'Img4'>
                        <img src = {img4}></img>
                    </div>
                    <div className = 'Img5'>
                        <img src = {img5}></img>
                    </div>
                    <div className = 'Img6'>
                        <img src = {img6}></img>
                    </div>
                </div>
            </div>
        );
    }
}