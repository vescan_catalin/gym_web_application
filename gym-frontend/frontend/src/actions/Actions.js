// import * as url from '../constants/FetchConstants'

var url = require('../constants/FetchConstants')
const fetch = require('node-fetch')

// save user in data base
const register = user => {
    return fetch(url.addUser, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(user)
    })
    .then(result => result.json())
};

// check if user exists
const login = user => {
    return fetch(url.checkUser, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(user)
    })
    .then(result => result.json())
}

// update users data
const changeData = data => {
    return fetch(url.updateUser, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(data)
    })
    .then(result => result.json())
}

// get all users
const getUsers = () => {
    return fetch(url.getUsers, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

const getUsersByType = type => {
    return fetch(url.getUsersByType + '/' + type, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// load schedule
const loadSchedule = () => {
    return fetch(url.loadSchedule, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// update schedule
const updateSchedule = data => {
    return fetch(url.updateSchedule, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(data)
    })
    .then(result => result.json())
}

// send mail
const sendEmail = (sendEmailToAllUsers, message) => {
    return fetch(url.sendEmail + '/' + message, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(sendEmailToAllUsers)
    })
} 

// get all chat messages from db
const getAllMessages = () => {
    return fetch(url.getAllMessages, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// send chat message to db
const sendMessage = data => {
    return fetch(url.sendMessage, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(data)
    })
    .then(result => result.json())
}

// get the list of offensive words 
const getOffensiveWords = () => {
    return fetch(url.messageFiltering, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// clear chat conversation
const clearChat = () => {
    return fetch(url.clearChat, {
        method: url.DELETE,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// add new subscription
const addSubscription = subscription => {
    return fetch(url.addSubscription, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(subscription)
    })
    .then(result => result.json())
}

// get all subscriptions
const getAllSubscriptions = () => {
    return fetch(url.getAllSubscriptions, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// update a subscription
const updateSubscription = subscription => {
    return fetch(url.updateSubscription, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(subscription)
    })
    .then(result => result.json())
}

// find subscription by a specific type
const findSubscriptionByType = subscriptionType => {
    return fetch(url.findByType + '/' + subscriptionType, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// create reservation
const addReservation = reservation => {
    return fetch(url.addReservation, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(reservation)
    })
    .then(result => result.json())
}

// get all reservations
const getReservations = () => {
    return fetch(url.getReservations, {
        method: url.GET,
        headers: url.headers,
        body: JSON.stringify()
    })
    .then(result => result.json())
}

// payment for subscription
const pay = customerDetails => {
    return fetch(url.payment, {
        method: url.POST,
        headers: url.headers,
        body: JSON.stringify(customerDetails)
    })
    .then(result => result.json())
}

module.exports = {register, login, changeData, getUsers, 
    loadSchedule, updateSchedule, sendEmail, getAllMessages, 
    sendMessage, getOffensiveWords, clearChat, addSubscription, 
    getAllSubscriptions, updateSubscription, getReservations,
    addReservation, findSubscriptionByType, pay, getUsersByType}