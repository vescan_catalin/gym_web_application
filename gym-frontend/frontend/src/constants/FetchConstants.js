// users url
const addUser = 'http://localhost:8080/api/user'
const checkUser = 'http://localhost:8080/api/user/check_user'
const updateUser = 'http://localhost:8080/api/user/update'
const getUsers = 'http://localhost:8080/api/user/all'
const getUsersByType = 'http://localhost:8080/api/user/usersByType'

// schedule url
const loadSchedule = 'http://localhost:8080/api/schedule/all'
const updateSchedule = 'http://localhost:8080/api/schedule/update'

// send email url
const sendEmail = 'http://localhost:8080/api/email/sendToAll'

// chat url
const getAllMessages = 'http://localhost:8080/api/chat/all'
const sendMessage = 'http://localhost:8080/api/chat/addMessage'
const messageFiltering = 'http://localhost:8080/api/chat/messageFiltering'
const clearChat = 'http://localhost:8080/api/chat/clearChat'

// subscriptions url
const getAllSubscriptions = 'http://localhost:8080/api/subscriptions/all'
const addSubscription = 'http://localhost:8080/api/subscriptions/addSubscription'
const updateSubscription = 'http://localhost:8080/api/subscriptions/update'
const findByType = 'http://localhost:8080/api/subscriptions'

// reservations
const addReservation = 'http://localhost:8080/api/reservations/add'
const getReservations = 'http://localhost:8080/api/reservations/all'

// payment
const payment = 'http://localhost:8080/api/stripe/payment'

// methods
const POST = 'POST'
const GET = 'GET'
const DELETE = 'DELETE'

// headers
const headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-type': 'application/json'
}

module.exports = {addUser, checkUser, updateUser, getUsers, 
    loadSchedule, updateSchedule, sendEmail, getAllMessages, 
    sendMessage, messageFiltering, clearChat, getAllSubscriptions,
    addSubscription, updateSubscription, addReservation, findByType,
    payment, getUsersByType, getReservations,
    POST, GET, DELETE, headers}