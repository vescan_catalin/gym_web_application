package com.example.test.service;

import com.example.test.entities.ClassType;
import com.example.test.repository.ClassTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassTypeService {

    @Autowired
    private ClassTypeRepository repository;

    public ClassType saveOrUpdateClassType(ClassType classType) {
        return repository.save(classType);
    }

    public Iterable<ClassType> findAll() {
        return repository.findAll();
    }

    public ClassType findClassTypeById(Long id) {
        return repository.findClassTypeById(id);
    }

    public ClassType findClassTypeByName(String name) {
        return repository.findClassTypeByName(name);
    }

    public void delete(Long id) {
        ClassType classType = findClassTypeById(id);
        repository.delete(classType);
    }


}
