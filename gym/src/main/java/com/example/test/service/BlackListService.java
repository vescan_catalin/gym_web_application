package com.example.test.service;

import com.example.test.entities.BlackList;
import com.example.test.repository.BlackListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BlackListService {

    @Autowired
    private BlackListRepository repository;

    public BlackList saveOrUpdateBlackList(BlackList blackList) {
        return repository.save(blackList);
    }

    public Iterable<BlackList> findAll() {
        return repository.findAll();
    }

    public BlackList findBlackListById(Long id) {
        return repository.findBlackListById(id);
    }

    public void delete(Long id) {
        BlackList blackList = findBlackListById(id);
        repository.delete(blackList);
    }
}
