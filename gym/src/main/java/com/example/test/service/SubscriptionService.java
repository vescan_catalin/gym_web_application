package com.example.test.service;

import com.example.test.entities.Subscription;
import com.example.test.repository.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionService {

    @Autowired
    private SubscriptionRepository repository;

    public Subscription saveOrUpdateSubscription(Subscription subscription) {
        return repository.save(subscription);
    }

    public Iterable<Subscription> findAll() {
        return repository.findAll();
    }

    public Subscription findSubscriptionById(Long id) {
        return repository.findSubscriptionById(id);
    }

    public void delete(Long id) {
        Subscription subscription = findSubscriptionById(id);
        repository.delete(subscription);
    }

}
