package com.example.test.service;

import com.example.test.entities.Chat;
import com.example.test.repository.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class ChatService {

    private final String filePath = "E:\\sisteme distribuite\\sd\\gym\\bad_words.txt";
    private List<String> offensiveWordsList = new ArrayList<>();

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ChatRepository repository;

    public Chat save(Chat message) {
        return repository.save(message);
    }

    public Iterable<Chat> findAll() {
        return repository.findAll();
    }

    @Transactional
    public void clearChat() {
        repository.deleteAll();

//        em.joinTransaction();
        em.createNativeQuery("alter table chat auto_increment = 1").executeUpdate();
    }

    public List<String> offensiveWords() {
        offensiveWordsList.clear();

        File file = new File(filePath);

        BufferedReader br = null;
        String fileContent = null;
        try {
            br = new BufferedReader(new FileReader(file));

            while((fileContent = br.readLine()) != null) {
                offensiveWordsList.add(fileContent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return offensiveWordsList;
    }

}
