package com.example.test.service;

import com.example.test.entities.SubscriptionList;
import com.example.test.repository.SubscriptionListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionListService {

    @Autowired
    private SubscriptionListRepository subscriptionListRepository;

    public SubscriptionList save(SubscriptionList subscriptionList) {
        return subscriptionListRepository.save(subscriptionList);
    }

    public Iterable<SubscriptionList> findAll() {
        return subscriptionListRepository.findAll();
    }

    public SubscriptionList findSubscriptionById(Long id) {
        return subscriptionListRepository.findSubscriptionListById(id);
    }

    public SubscriptionList findSubscriptionByType(String subsctriptionType) {
        return subscriptionListRepository.findSubscriptionListByType(subsctriptionType);
    }

    public SubscriptionList updateSubscription(SubscriptionList subscription) {
        SubscriptionList subs = subscriptionListRepository.findSubscriptionListById(subscription.getId());

        subs.setType(subscription.getType() != null ? subscription.getType() : subs.getType());
        subs.setDescription(subscription.getDescription() != null ? subscription.getDescription() : subs.getDescription());
        subs.setPrice(subscription.getPrice() != 0 ? subscription.getPrice() : subs.getPrice());

        return subscriptionListRepository.save(subs);
    }

    public void deleteSubscription(Long id) {
        SubscriptionList subscriptionList = subscriptionListRepository.findSubscriptionListById(id);

        subscriptionListRepository.delete(subscriptionList);
    }

}
