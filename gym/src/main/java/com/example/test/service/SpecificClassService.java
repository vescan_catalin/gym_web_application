package com.example.test.service;

import com.example.test.entities.SpecificClass;
import com.example.test.repository.SpecificClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpecificClassService {

    @Autowired
    private SpecificClassRepository repository;

    public SpecificClass saveOrUpdateSpecificClass(SpecificClass specificClass) {
        return repository.save(specificClass);
    }

    public Iterable<SpecificClass> findAll() {
        return repository.findAll();
    }

    public SpecificClass findSpecificClassById(Long id) {
        return repository.findSpecificClassById(id);
    }

    public void delete(Long id) {
        SpecificClass specificClass = findSpecificClassById(id);
        repository.delete(specificClass);
    }


}
