package com.example.test.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.*;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StripeService {
    private  Gson gson;

    public Customer createCustomer(String email, String name) {
        Map<String, Object> customerParameters = new HashMap<String, Object>();
        customerParameters.put("email", email);
        customerParameters.put("name", name);

        Customer newCustomer = null;
        try {
            newCustomer = Customer.create(customerParameters);
        } catch (StripeException e) {
            e.printStackTrace();
        }

        return newCustomer;
    }

    public Map<String, Object> createCard(String name, String number, String expMonth, String expYear, String cvc) {
        Map<String, Object> cardParameters = new HashMap<String, Object>();
        cardParameters.put("name", name);
        cardParameters.put("number", number);
        cardParameters.put("exp_month", expMonth);
        cardParameters.put("exp_year", expYear);
        cardParameters.put("cvc", cvc);

        return cardParameters;
    }

    public Token createToken(Map<String, Object> cardParameters) {
        Map<String, Object> tokenParameter = new HashMap<String, Object>();
        tokenParameter.put("card", cardParameters);

        Token token = null;
        try {
            token = Token.create(tokenParameter);
        } catch (StripeException e) {
            e.printStackTrace();
        }

        return token;
    }

    public void createSource(Customer customer, Token token) {
        Map<String, Object> source = new HashMap<String, Object>();
        source.put("source", token.getId());

        try {
            customer.getSources().create(source);
        } catch (StripeException e) {
            e.printStackTrace();
        }
    }

    public Charge createCharge(Customer customer, String amount, String currency) {
        Map<String, Object> chargeParameters = new HashMap<String, Object>();
        chargeParameters.put("amount", amount);
        chargeParameters.put("currency", currency);
        chargeParameters.put("customer", customer.getId());
        Charge charge = null;
        try {
            charge = Charge.create(chargeParameters);
        } catch (StripeException e) {
            e.printStackTrace();
        }

        return charge;
    }

    public List<Customer> getCustomers() {
        Map<String, Object> customersAccount = new HashMap<String, Object>();
        customersAccount.put("limit", "100");

        List<Customer> customers = null;
        try {
            customers = Customer.list(customersAccount).getData();
        } catch (StripeException e) {
            e.printStackTrace();
        }

        return customers;
    }

    public Customer createAll(Map<String, String> customerDetails) {
        // create customer
        Customer newCustomer = createCustomer(customerDetails.get("email"), customerDetails.get("name"));

        // create card
        Map<String, Object> card = createCard(customerDetails.get("name"),
                customerDetails.get("cardNumber"),
                customerDetails.get("expirationMonth"),
                customerDetails.get("expirationYear"),
                customerDetails.get("cvc"));

        // create token for this card
        Token token = createToken(card);

        // create source
        createSource(newCustomer, token);

        return newCustomer;
    }

    public String payment(Map<String, String> customerDetails) {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        Stripe.apiKey = "sk_test_JBbrEjgzwaWEJCRuTcHODawd00Kc8JAJVn";
        gson = new GsonBuilder().setPrettyPrinting().create();

        Boolean cardExists = false;
        List<Customer> customers = getCustomers();
        Customer chargeThisCustomer = null;
        Charge charge = null;

        try {
            for (Customer customer : customers) {
                PaymentSourceCollection customerCardDetails = Customer.retrieve(customer.getId()).getSources();

                for(PaymentSource eachCustomerCardDetails : customerCardDetails.getData()) {
                    String cardDetails = gson.toJson(eachCustomerCardDetails);
                    Card card = gson.fromJson(cardDetails, Card.class);

                    if(card.getName().equals(customerDetails.get("name"))) {
                        cardExists = true;
                        chargeThisCustomer = customer;
                        break;
                    } else {
                        continue;
                    }
                }

                if(cardExists)
                    break;
            }

            if(chargeThisCustomer != null) {
                if (cardExists) {
                    // create charge
                    charge = createCharge(chargeThisCustomer, customerDetails.get("amount"), customerDetails.get("currency"));

                    return charge.getStatus();
                } else {
                    createAll(customerDetails);

                    // create charge
                    charge = createCharge(chargeThisCustomer, customerDetails.get("amount"), customerDetails.get("currency"));

                    return charge.getStatus();
                }
            } else {
                chargeThisCustomer = createAll(customerDetails);

                // create charge
                charge = createCharge(chargeThisCustomer, customerDetails.get("amount"), customerDetails.get("currency"));

                return charge.getStatus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "something went wrong";
    }

}
