package com.example.test.service;

import com.example.test.entities.User;
import com.example.test.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public User save(User user) {
        return repository.save(user);
    }

    public Iterable<User> findAll() {
        return repository.findAll();
    }

    public Iterable<User> findUsersByType(String type) {
        return repository.findUserByType(type);
    }

    public User findUserById(Long id) {
        return repository.findUserById(id);
    }

    public User findUserByName(String name) {
        return repository.findUserByName(name);
    }

    public User updateUser(User user) {
        User u = repository.findUserByName(user.getName());

        u.setPassword(user.getPassword());
        u.setEmail(user.getEmail());

        return repository.save(u);
    }

    public void delete(Long id) {
        User user = findUserById(id);
        repository.delete(user);
    }

}
