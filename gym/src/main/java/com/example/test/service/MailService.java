package com.example.test.service;

import com.example.test.entities.User;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Service
public class MailService {

    public synchronized String sendMail(User user, String message) {
        Properties properties = new Properties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
//        properties.put("mail.debug", "false");

        Session session = Session.getInstance(properties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("vescan.catalin96@gmail.com", "Algocalmin.666");
                    }
                });
        session.setDebug(true);

        Message mimeMessage = new MimeMessage(session);
        try {
            mimeMessage.setFrom(new InternetAddress("vescan.catalin96@gmail.com", false));
            mimeMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(user.getEmail()));
            mimeMessage.setSubject("My Gym");
            mimeMessage.setContent(message, "text/html");
            mimeMessage.setSentDate(new Date());

            Transport.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return "Email successfully sent!";
    }

    @Async
    public String sendMailToAllUsers(List<User> users, String message) {

//        performance test for asynchronous execution
        //long start = System.currentTimeMillis();

        // asynchronously call for email sending

        users.parallelStream().forEach(x -> sendMail(x, message));

        //System.out.println(System.currentTimeMillis() - start + "ms");

        return "Emails successfully sent!";
    }
}
