package com.example.test.service;

import com.example.test.entities.Reservations;
import com.example.test.repository.ReservationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationsService {

    @Autowired
    private ReservationsRepository repository;

    public Reservations saveOrUpdateReservations(Reservations reservations) {
        return repository.save(reservations);
    }

    public Iterable<Reservations> findAll() {
        return repository.findAll();
    }

    public Reservations findReservationsById(Long id) {
        return repository.findReservationsById(id);
    }

    public void delete(Long id) {
        Reservations reservations = findReservationsById(id);
        repository.delete(reservations);
    }
}
