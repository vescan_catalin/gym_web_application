package com.example.test.service;

import com.example.test.entities.Schedule;
import com.example.test.repository.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepository repository;

    public Schedule saveSchedule(Schedule schedule) {
        return repository.save(schedule);
    }

    public Schedule findScheduleById(Long id) {
        return repository.findScheduleById(id);
    }

    public Iterable<Schedule> findAll() {
        return repository.findAll();
    }

    public Schedule updateSchedule(Schedule schedule) {
        Schedule newSch = repository.findScheduleById(schedule.getId());

        newSch.setText(schedule.getText());

        return repository.save(newSch);
    }

    public void delete(Long id) {
        Schedule schedule = repository.findScheduleById(id);

        repository.delete(schedule);
    }
}
