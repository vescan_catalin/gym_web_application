package com.example.test.controller;

import com.example.test.entities.SpecificClass;
import com.example.test.service.SpecificClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/specific_class")
@CrossOrigin
public class SpecificClassController {

    @Autowired
    private SpecificClassService service;

    @PostMapping("")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addSpecificClass (@Valid @RequestBody SpecificClass specificClass, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError error : result.getFieldErrors())
                errorMap.put(error.getField(), error.getDefaultMessage());

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        SpecificClass newSpecificClass = service.saveOrUpdateSpecificClass(specificClass);

        return new ResponseEntity<SpecificClass>(newSpecificClass, HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<SpecificClass> getAll() {
        return service.findAll();
    }


}
