package com.example.test.controller;

import com.example.test.entities.Chat;
import com.example.test.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/chat")
@CrossOrigin
public class ChatController {

    @Autowired
    private ChatService service;

    @GetMapping("/all")
    public Iterable<Chat> getAllMessages() {
        return service.findAll();
    }

    @PostMapping("/addMessage")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addMessage(@Valid @RequestBody Chat message, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<> ();

            for(FieldError err: result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        Chat newChat = service.save(message);

        return new ResponseEntity<Chat>(newChat, HttpStatus.OK);
    }

    @DeleteMapping("/clearChat")
    public ResponseEntity<?> clearChat() {
        service.clearChat();

        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }

    @GetMapping("/messageFiltering")
    public ResponseEntity<?> messageFiltering() {
        List<String> offensiveWords = service.offensiveWords();

        return new ResponseEntity<List<String>>(offensiveWords, HttpStatus.OK);
    }

}
