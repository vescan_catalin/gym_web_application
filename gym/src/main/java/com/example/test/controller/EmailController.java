package com.example.test.controller;

import com.example.test.entities.User;
import com.example.test.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/email")
@CrossOrigin
public class EmailController {

    @Autowired
    private MailService service;

    @PostMapping("/send/{message}")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> sendMail(@Valid @RequestBody User user,
                                      @PathVariable String message, BindingResult result) {
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        String response = service.sendMail(user, message);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    @PostMapping("/sendToAll/{message}")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> sendMailToAllUsers(@Valid @RequestBody List<User> users,
                                                @PathVariable String message, BindingResult result) {
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        String response = service.sendMailToAllUsers(users, message);

        return new ResponseEntity<String>(response, HttpStatus.OK);
    }
}
