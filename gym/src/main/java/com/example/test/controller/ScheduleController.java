package com.example.test.controller;

import com.example.test.entities.Schedule;
import com.example.test.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/schedule")
@CrossOrigin
public class ScheduleController {

    @Autowired
    private ScheduleService service;

    @PostMapping("")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> saveSchedule(@Valid @RequestBody Schedule schedule, BindingResult result) {
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        Schedule newSchedule = service.saveSchedule(schedule);

        return new ResponseEntity<Schedule>(newSchedule, HttpStatus.OK);
    }

    @GetMapping("/all")
    @SuppressWarnings("Dupticates")
    public Iterable<Schedule> getAllSchedule() {
        return service.findAll();
    }

    @GetMapping("/{scheduleId}")
    public ResponseEntity<?> getScheduleById(@PathVariable Long scheduleId) {
        Schedule cellSchedule = service.findScheduleById(scheduleId);

        return new ResponseEntity<Schedule>(cellSchedule, HttpStatus.OK);
    }

    @PostMapping("/update")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> updateSchedule(@Valid @RequestBody Schedule schedule, BindingResult result) {
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        Schedule updatedSchedule = service.updateSchedule(schedule);

        return new ResponseEntity<Schedule>(updatedSchedule, HttpStatus.OK);
    }

    @DeleteMapping("/{scheduleId")
    public ResponseEntity<?> deleteScheduleById(@PathVariable Long scheduleId) {
        service.delete(scheduleId);

        return new ResponseEntity<String>("Successfully deleted!", HttpStatus.OK);
    }

}
