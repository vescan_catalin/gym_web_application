package com.example.test.controller;

import com.example.test.entities.ClassType;
import com.example.test.service.ClassTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/class_type")
@CrossOrigin
public class ClassTypeController {

    @Autowired
    private ClassTypeService service;

    @PostMapping("")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addClassType(@Valid @RequestBody ClassType classType, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError error : result.getFieldErrors())
                errorMap.put(error.getField(), error.getDefaultMessage());

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        ClassType newClassType = service.saveOrUpdateClassType(classType);

        return new ResponseEntity<ClassType>(newClassType, HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<ClassType> getAll() {
        return service.findAll();
    }


}
