package com.example.test.controller;

import com.example.test.entities.Subscription;
import com.example.test.entities.SubscriptionList;
import com.example.test.entities.User;
import com.example.test.service.StripeService;
import com.example.test.service.SubscriptionListService;
import com.example.test.service.SubscriptionService;
import com.example.test.service.UserService;
import com.google.gson.Gson;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/stripe")
@CrossOrigin
public class StripeController {

    @Autowired
    private StripeService service;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private SubscriptionListService subscriptionListService;

    @Autowired
    private UserService userService;

    @PostMapping("/payment")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> payment(@Valid @RequestBody Map<String, String> customerDetails, BindingResult result) {
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        String response = service.payment(customerDetails);

        if(response.equals("succeeded")) {
            SubscriptionList subscriptionList = subscriptionListService.findSubscriptionByType(customerDetails.get("subscription"));
            User user = userService.findUserByName(customerDetails.get("user"));

            // set subscription validity
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-YYYY");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());

            String validity = null;
            if (customerDetails.get("subscription").equals("1 year pass")) {
                calendar.add(Calendar.YEAR, 1);
                validity = simpleDateFormat.format(calendar.getTime());
            } else if (customerDetails.get("subscription").equals("1 day pass")) {
                calendar.add(Calendar.DAY_OF_MONTH, 1);
                validity = simpleDateFormat.format(calendar.getTime());
            } else {
                calendar.add(Calendar.MONTH, 1);
                validity = simpleDateFormat.format(calendar.getTime());
            }

            Long subscriptionListId = subscriptionList.getId();
            Long userId = user.getId();
            String type = subscriptionList.getType();

            Subscription subscription = new Subscription(subscriptionListId, userId, validity, type);
            Subscription newSubscription = subscriptionService.saveOrUpdateSubscription(subscription);

            // generate PDF
            Document document = new Document();
            PdfWriter pdfWriter = null;
            String raport = "";

            try {
                pdfWriter = pdfWriter.getInstance(document, new FileOutputStream("Subscriptions.pdf"));
                document.open();
                raport += "nume utilizator: " + customerDetails.get("name") + "\n";
                raport += "numele de pe card: " + customerDetails.get("name") + "\n";
                raport += "numar card: " + customerDetails.get("cardNumber") + "\n";
                raport += "data expirarii: " +  customerDetails.get("expirationMonth") + "/" +
                            customerDetails.get("expirationYear") + "\n";
                raport += "tipul abonamentului: " + customerDetails.get("subscription") + "\n";
                int amount = Integer.parseInt(customerDetails.get("amount"))/100;
                raport += "suma platita: " + amount + " " + customerDetails.get("currency") + "\n";
                document.add(new Paragraph(raport));
                document.close();
                pdfWriter.close();
            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            return new ResponseEntity<Subscription>(newSubscription, HttpStatus.OK);
        }

        return new ResponseEntity<String>("something went wrong", HttpStatus.BAD_REQUEST);
    }

}
