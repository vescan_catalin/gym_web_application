package com.example.test.controller;

import com.example.test.entities.Subscription;
import com.example.test.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/subscription")
@CrossOrigin
public class SubscriptionController {

    @Autowired
    private SubscriptionService service;

    @PostMapping("")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addSubscription(@Valid @RequestBody Subscription subscription, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors())
                errorMap.put(err.getField(), err.getDefaultMessage());

            return  new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        Subscription newSubscription = service.saveOrUpdateSubscription(subscription);

        return new ResponseEntity<Subscription>(newSubscription, HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<Subscription> getAll() {
        return service.findAll();
    }


}
