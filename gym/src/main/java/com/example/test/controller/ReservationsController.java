package com.example.test.controller;

import com.example.test.entities.ClassType;
import com.example.test.entities.Reservations;
import com.example.test.entities.SpecificClass;
import com.example.test.entities.User;
import com.example.test.service.ClassTypeService;
import com.example.test.service.ReservationsService;
import com.example.test.service.SpecificClassService;
import com.example.test.service.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectSerializer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/reservations")
@CrossOrigin
public class ReservationsController {

    @Autowired
    private ReservationsService service;

    @Autowired
    private ClassTypeService classTypeService;

    @Autowired
    private UserService userService;

    @Autowired
    private SpecificClassService specificClassService;

    @PostMapping("/add")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addReservations(@Valid @RequestBody LinkedHashMap reservation, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError error : result.getFieldErrors())
                errorMap.put(error.getField(), error.getDefaultMessage());

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        String className = reservation.get("class").toString();
        String userName = reservation.get("user").toString();
        String date = reservation.get("date").toString();

        User user = userService.findUserByName(userName);
        ClassType classType = classTypeService.findClassTypeByName(className);

        List<Reservations> allReservations = (List<Reservations>) service.findAll();

        Long exists = allReservations.stream().filter(eachReservation ->
                eachReservation.getUserId() == user.getId() &&
                eachReservation.getDate().equals(date)).count();

        if(exists == 0) {
            SpecificClass specificClass = new SpecificClass();
            specificClass.setUserId(user.getId());
            specificClass.setClassTypeId(classType.getId());

            specificClass = specificClassService.saveOrUpdateSpecificClass(specificClass);

            Reservations reservations = new Reservations();
            reservations.setSpecificClassId(specificClass.getId());
            reservations.setUserId(user.getId());
            reservations.setDate(date);

            Reservations newReservations = service.saveOrUpdateReservations(reservations);

            return new ResponseEntity<Reservations>(newReservations, HttpStatus.OK);
        }

        return new ResponseEntity<HttpStatus>(HttpStatus.IM_USED, HttpStatus.IM_USED);
    }

    @GetMapping("/all")
    public List<Map<String, String>> getAll() {
        Iterable<Reservations> reservations = service.findAll();
        List<Map<String, String>> toReturn = new ArrayList<Map<String, String>>();

        User user = null;
        SpecificClass specificClass = null;
        ClassType classType = null;

        for (Reservations reservation : reservations) {
            Map<String, String> res = new HashMap<String, String>();
            specificClass = specificClassService.findSpecificClassById(reservation.getSpecificClassId());
            classType = classTypeService.findClassTypeById(specificClass.getClassTypeId());
            user = userService.findUserById(reservation.getUserId());

            res.put("username", user.getName());
            res.put("class", classType.getName());
            res.put("date", reservation.getDate());

            toReturn.add(res);
        }
        return toReturn;
    }


}
