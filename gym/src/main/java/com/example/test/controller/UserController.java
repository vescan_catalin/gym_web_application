package com.example.test.controller;

import com.example.test.entities.User;
import com.example.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService service;

    @PostMapping("")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addUser(@Valid @RequestBody User user, BindingResult result) {
       if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        User newUser = service.save(user);

        return new ResponseEntity<User>(newUser, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public Iterable<User> getAllUsers() {
        return service.findAll();
    }

    @GetMapping("/usersByType/{user_type}")
    public Iterable<User> getUsersByType(@PathVariable String user_type) {
        return service.findUsersByType(user_type);
    }

    @GetMapping("/{user_id}")
    public ResponseEntity<?> getUserById(@PathVariable Long user_id) {
        User user = service.findUserById(user_id);

        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @DeleteMapping("/{user_id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long user_id) {
        service.delete(user_id);

        return new ResponseEntity<String>("successfully deleted", HttpStatus.OK);
    }

    @PostMapping("/update")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> updateUser(@Valid @RequestBody User user, BindingResult result) {
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        User updatedUser = service.updateUser(user);

        return new ResponseEntity<User>(updatedUser, HttpStatus.OK);
    }

    @PostMapping("/check_user")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> loginValidation(@Valid @RequestBody User user, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        List<User> users = (List<User>) service.findAll();

        User validUser = users.stream()
                              .filter(eachUser -> eachUser.getName().equals(user.getName()) &&
                                      eachUser.getPassword().equals(user.getPassword()))
                              .findFirst()
                              .get();

        return new ResponseEntity<User>(validUser, HttpStatus.OK);
    }
}
