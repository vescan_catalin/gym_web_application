package com.example.test.controller;

import com.example.test.entities.SubscriptionList;
import com.example.test.service.SubscriptionListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/subscriptions")
@CrossOrigin
public class SubscriptionListController {

    @Autowired
    private SubscriptionListService service;

    @PostMapping("/addSubscription")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addSubscription(@Valid @RequestBody SubscriptionList subscriptionList,
                                             BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError err : result.getFieldErrors()) {
                errorMap.put(err.getField(), err.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        SubscriptionList newSubsList = service.save(subscriptionList);

        return new ResponseEntity<SubscriptionList>(newSubsList, HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public Iterable<SubscriptionList> getAllSubscriptions() {
        return service.findAll();
    }

//    @GetMapping("/{subscriptions_id}")
//    public ResponseEntity<?> getSubscriptionById(@PathVariable Long subscriptions_id) {
//        SubscriptionList subscriptionList = service.findSubscriptionById(subscriptions_id);
//
//        return new ResponseEntity<SubscriptionList>(subscriptionList, HttpStatus.OK);
//    }

    @GetMapping("/{subscription_type}")
    public ResponseEntity<?> getSubscriptionByName(@PathVariable String subscription_type) {
        SubscriptionList subscriptionList = service.findSubscriptionByType(subscription_type);

        return new ResponseEntity<SubscriptionList>(subscriptionList, HttpStatus.OK);
    }

    @DeleteMapping("/{subscription_id}")
    public ResponseEntity<?> deleteSubscription(@PathVariable Long subscription_id) {
        service.deleteSubscription(subscription_id);

        return new ResponseEntity<String>("successfully deleted", HttpStatus.OK);
    }

    @PostMapping("/update")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> updateSubscription(@Valid @RequestBody SubscriptionList subscriptionList,
                                                BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError error : result.getFieldErrors()) {
                errorMap.put(error.getField(), error.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        SubscriptionList updatedSubscription = service.updateSubscription(subscriptionList);

        return new ResponseEntity<SubscriptionList>(updatedSubscription, HttpStatus.OK);
    }
}
