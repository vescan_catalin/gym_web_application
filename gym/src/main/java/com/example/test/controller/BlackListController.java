package com.example.test.controller;

import com.example.test.entities.BlackList;
import com.example.test.service.BlackListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/black_list")
@CrossOrigin
public class BlackListController {

    @Autowired
    private BlackListService service;

    @PostMapping("")
    @SuppressWarnings("Duplicates")
    public ResponseEntity<?> addToBlockList(@Valid @RequestBody BlackList blackList, BindingResult result) {
        if(result.hasErrors()) {
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError error : result.getFieldErrors())
                errorMap.put(error.getField(), error.getDefaultMessage());

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }

        BlackList newBlackList = service.saveOrUpdateBlackList(blackList);

        return new ResponseEntity<BlackList>(newBlackList, HttpStatus.OK);
    }

    @GetMapping("/all")
    public Iterable<BlackList> getAll() {
        return service.findAll();
    }
}
