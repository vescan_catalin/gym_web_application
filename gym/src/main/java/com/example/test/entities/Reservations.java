package com.example.test.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "reservations")
public class Reservations implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private Long specificClassId;
    private String date;

    public Reservations() {
    }

    public Reservations(Long userId, Long specificClassId) {
        this.userId = userId;
        this.specificClassId = specificClassId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "specific_class_id")
    public Long getSpecificClassId() {
        return specificClassId;
    }

    public void setSpecificClassId(Long specificClassId) {
        this.specificClassId = specificClassId;
    }

    @Basic
    @Column(name = "date")
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
