package com.example.test.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "chat")
public class Chat implements Serializable {

    private Long id;

    private String sender;
    private String message;

    public Chat() {

    }

    public Chat(String sender, String message) {
        this.sender = sender;
        this.message = message;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sender")
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Basic
    @Column(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
