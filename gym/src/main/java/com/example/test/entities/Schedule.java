package com.example.test.entities;

import java.io.Serializable;
import javax.persistence.*;


@Entity
@Table(name = "schedule")
public class Schedule implements Serializable {

    private Long id;

    private String text;

    public Schedule() {}

    public Schedule(Long id, String text) {
        this.id = id;
        this.text = text;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", text='" + text + '\'' +
                '}';
    }
}
