package com.example.test.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "subscription")
public class Subscription implements Serializable {

    private Long id;
    private Long subscriptionListId;
    private Long userId;
    private String type;
    private String validity;

    public Subscription() {
    }

    public Subscription(Long subscriptionListId, Long userId, String validity, String type) {
        this.subscriptionListId = subscriptionListId;
        this.userId = userId;
        this.type = type;
        this.validity = validity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "subscriptionlist_id")
    public Long getSubscriptionListId() {
        return subscriptionListId;
    }

    public void setSubscriptionListId(Long subscriptionListId) {
        this.subscriptionListId = subscriptionListId;
    }

    @Basic
    @Column(name = "user_id")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "validity")
    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }
}
