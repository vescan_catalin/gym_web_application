package com.example.test.repository;

import com.example.test.entities.SpecificClass;
import com.example.test.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecificClassRepository extends JpaRepository<SpecificClass, Long> {
    public SpecificClass findSpecificClassById(Long id);
}
