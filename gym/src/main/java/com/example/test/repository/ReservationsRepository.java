package com.example.test.repository;

import com.example.test.entities.Reservations;
import com.example.test.entities.SpecificClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationsRepository extends JpaRepository<Reservations, Long> {
    public Reservations findReservationsById(Long id);

}
