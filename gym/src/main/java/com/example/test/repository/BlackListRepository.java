package com.example.test.repository;

import com.example.test.entities.BlackList;
import com.example.test.entities.SpecificClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BlackListRepository extends JpaRepository<BlackList, Long> {
    public BlackList findBlackListById(Long id);

}
