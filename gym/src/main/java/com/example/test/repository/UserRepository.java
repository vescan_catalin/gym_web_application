package com.example.test.repository;

import com.example.test.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findUserById(Long id);
    public User findUserByName(String userName);
    public Iterable<User> findUserByType(String type);
}
