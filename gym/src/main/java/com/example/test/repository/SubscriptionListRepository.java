package com.example.test.repository;

import com.example.test.entities.SubscriptionList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionListRepository extends JpaRepository<SubscriptionList, Long> {
    public SubscriptionList findSubscriptionListById(Long id);
    public SubscriptionList findSubscriptionListByType(String subscriptionType);
}
