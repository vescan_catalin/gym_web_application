package com.example.test.repository;

import com.example.test.entities.Subscription;
import com.example.test.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    public Subscription findSubscriptionById(Long id);
}
