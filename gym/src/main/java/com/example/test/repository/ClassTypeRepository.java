package com.example.test.repository;

import com.example.test.entities.ClassType;
import com.example.test.entities.SpecificClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClassTypeRepository extends JpaRepository<ClassType, Long> {
    public ClassType findClassTypeById(Long id);
    public ClassType findClassTypeByName(String name);
}
